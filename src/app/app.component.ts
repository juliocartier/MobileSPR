import { CameraPage } from './../pages/camera/camera';
import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
//import { Hello } from 'cordova-plugin-hello';
//declare let Hello: any;

import { HomePage } from '../pages/home/home';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = CameraPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    /*var sucess = function(message){
      alert(message);
    }

    var failure = function(){
      alert("Error Hello Plugin");
    }*/
    
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      statusBar.styleDefault();
      splashScreen.hide();
      //Hello.greet("Greet", sucess, failure);
    });
  }
}

