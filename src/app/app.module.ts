//import { MediaCapture } from '@ionic-native/media-capture';
import { CameraPreview } from '@ionic-native/camera-preview';
import { CameraPageModule } from './../pages/camera/camera.module';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, ToastController } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';


//import { FusionCharts } from 'fusioncharts';
//import { Camera, CameraOptions } from '@ionic-native/camera';

import { ChartModule } from 'angular2-highcharts';
import * as HighCharts from 'highcharts';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';


@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    CameraPageModule,
    IonicModule.forRoot(MyApp),
    ChartModule.forRoot(HighCharts)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    CameraPreview,
    ToastController,
    //MediaCapture,
   // Camera,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
