import { Platform, DateTime } from 'ionic-angular';
import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, FabContainer, AlertController } from 'ionic-angular';
import { CameraPreview, CameraPreviewPictureOptions, CameraPreviewOptions } from '@ionic-native/camera-preview';
import * as HighCharts from 'highcharts';
import * as $ from "jquery";
import { MarvinImage } from 'marvin';
import { Filter, GrayScaleFilter } from '../../app/models/filter';
import { median } from 'filters';


/**
 * Generated class for the CameraPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-camera',
    templateUrl: 'camera.html',
})

export class CameraPage {
    chartSensorgrama: any;
    graficoAIM: any;
    public fab: FabContainer;
    public disabled = false; //Variável que habilita e disabilita o ion-range
    result1: any;
    minimo: any;
    maximo: any;
    assimetria: any;
    largura: any;
    testRadioOpen: any; //variavel do Radio para dialogo

    originalImg: MarvinImage;  // original image
    outputImg: MarvinImage;  // modified image by the filters
    filters: Filter[] = [];  // filters to bind in the list

    picture: string;
    cameraOpts: CameraPreviewOptions = {
        x: 200,
        y: 200,
        width: 50,
        height: 50,
        toBack: false,
        previewDrag: true,
    };
    cameraPictureOpts: CameraPreviewPictureOptions = {
        width: 250,
        height: 250,
        quality: 100
    };

    currentIndicesDryCell = [];

    comprimentoDeOn;

    /*
        Constantes para converter o comprimento de onda em uma
        cor rgb.   
    */
    Gamma = 0.80;
    IntensidadeMax = 255;
    data: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, public cameraPreview: CameraPreview, public platform: Platform, public ngZone: NgZone, private alertCtrl: AlertController) {
        //this.loadImage();
        //this.itemSelected();
        //this.getPointSpr(this.outputImg);
    }


    ionViewDidLoad() {
        this.graficoSensorgrama();
        this.chartAIM();
        this.startCamera();
        this.setupFilters();
    }

    /**
     * Grafico no modo AIM, com o tipo de gráfico
     * de dispersão para o eixo x e y, o eixo x é o ângulo
     * e o y é o indice de refletividade. A biblioteca utilizada foi Highcharts
     * 
     */
    async chartAIM() {
        var mainchart;

        this.graficoAIM = HighCharts.chart("containerAIM", {
            chart: {
                type: 'scatter',
                zoomType: 'xy',
                height: 200,
                backgroundColor: 'black',
                events: {
                    load: function () {
                        mainchart = this; // `this` is the reference to the chart
                    }
                }
            },
            yAxis: {
                title: {
                    text: "Refletividade",
                    style: { "color": "#ffffff" },
                },
                max: 1.2,
                min: 0
            },
            xAxis: {
                title: {
                    text: "Ângulo",
                    style: { "color": "#ffffff" }
                }
            },
            title: {
                text: 'Curva AIM',
                style: { "color": "#ffffff" },
            },
            plotOptions: {
                series: {
                    allowPointSelect: true,
                    color: 'rgba(223,83,83, .5)',
                    showInLegend: false
                }
            }, series: [{
                color: 'rgba(223,83,83, .5)',
                name: 'Ângulo'
            }, {
                color: 'rgba(255, 255, 255, .9)',
                name: 'Referência'

            }]
        });

    }

    play() {

        this.startCamera();
    }

    /**
     * Grafico Sensorgrama, com um eixo x e y, gráfico de linha
     * é necessário de um método que atualize os dados a uma
     * quantidade de ms.
     */
    async graficoSensorgrama() {
        this.chartSensorgrama = HighCharts.chart('container', {
            chart: {
                type: 'spline',
                marginRight: 5,
                height: 200,
                backgroundColor: 'black',
                events: {
                    load: function () {

                    }
                }
            },

            title: {
                text: 'Sensorgrama',
                style: { "color": "#ffffff" }
            },
            xAxis: {
                title: {
                    text: "Tempo",
                    style: { "color": "#ffffff" }
                },
                type: 'datetime',
                tickPixelInterval: 150
            },
            yAxis: {
                title: {
                    text: 'pixel',
                    style: { "color": "#ffffff" }
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#ffffff'
                }]
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br/>',
                pointFormat: '{point.x:%S}<br/>{point.y:.2f}'
            },
            legend: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            series: [{

            }]
        });

    }

    clear() {
        this.chartSensorgrama.series[0].setData([], true);
    }

    /**
     * Ajustar a cor para um comprimento de onda
     * tem uma condição para o ajuste.
     * @param color 
     * @param factor 
     */

    public ajusteColor(color: any, factor: any) {
        let result;
        if (color == 0.0) {
            result = 0;
        } else {
            result = Math.round(this.IntensidadeMax * Math.pow(color * factor, this.Gamma));
        }

        return result;
    }

    /**
     * Converte um valor numérico [nanometros] em uma string [RGB]
     * e altera o style do card-color [emissor de luz] com parâmetros
     * em RGB (ex: FF:FF:FF) a partir do valor atual do *@param comprimentoDeOnda . 
     * Afunção ainda retorna os valores. 
     */
    cor(comprimentoDeOnda) {

        this.comprimentoDeOn = comprimentoDeOnda.value;
        let red: number;
        let green: number;
        let blue: number;
        let SSS: number;

        if (comprimentoDeOnda.value >= 380 && comprimentoDeOnda.value < 440) {
            red = -(comprimentoDeOnda.value - 440) / (440 - 380);
            green = 0;
            blue = 1;
        } else if (comprimentoDeOnda.value >= 440 && comprimentoDeOnda.value < 490) {
            red = 0;
            green = (comprimentoDeOnda.value - 440) / (490 - 440);
            blue = 1;
        } else if (comprimentoDeOnda.value >= 490 && comprimentoDeOnda.value < 510) {
            red = 0;
            green = 1;
            blue = -(comprimentoDeOnda.value - 510) / (510 - 490);
        } else if (comprimentoDeOnda.value >= 510 && comprimentoDeOnda.value < 580) {
            red = (comprimentoDeOnda.value - 510) / (580 - 510);
            green = 1;
            blue = 0;
        } else if (comprimentoDeOnda.value >= 580 && comprimentoDeOnda.value < 645) {
            red = 1;
            green = -(comprimentoDeOnda.value - 645) / (645 - 580);
            blue = 0;
        } else if (comprimentoDeOnda.value >= 645 && comprimentoDeOnda.value <= 780) {
            red = 1;
            green = 0;
            blue = 0;
        } else {
            red = 255;
            green = 255;
            blue = 255;
        }

        if (comprimentoDeOnda.value >= 380 && comprimentoDeOnda.value < 420) {
            SSS = 0.3 + 0.7 * (comprimentoDeOnda.value - 350) / (420 - 350);
        } else if (comprimentoDeOnda.value >= 420 && comprimentoDeOnda.value <= 700) {
            SSS = 1.0;
        } else if (comprimentoDeOnda.value > 700 && comprimentoDeOnda.value <= 780) {
            SSS = 0.3 + 0.7 * (780 - comprimentoDeOnda.value) / (780 - 700);
        } else {
            SSS = 1;
        }

        SSS *= 255;
        let r: number;
        let g: number;
        let b: number;

        r = Math.floor(SSS * red);
        g = Math.floor(SSS * green);
        b = Math.floor(SSS * blue);

        let teste = document.getElementById("card-color");
        teste.style.backgroundColor = `rgb(${r},${g}, ${b})`;

        return { r, g, b };
    }



    /**
     *
     *
     * @param {*} oc
     * @param {FabContainer} fab
     * @memberof CameraPage
     * 
     * A função funciona recebendo como parametro se a camera está aberta ou não.
     * A variavel oc significa abrir e fechar, caso receba o valor opn apresentará na tela
     * a camera, caso não, some a camera da tela.
     */
    /*async startCloseCamera(oc: any, fab: FabContainer) {
        fab.close();
        if (oc == "opn") {
            this.picture = null;
            this.cameraPreview.startCamera(this.cameraOpts).then(
                (res) => {
                    console.log(res)
                    //this.result1 = await this.cameraPreview.setColorEffect('mono');
                    this.takePicture(); // tira foto assim que camera estiver pronto
                },
                (err) => {
                    console.log(err)
                });
            //const result = await this.cameraPreview.startCamera(this.cameraOpts);
            //this.result1 = await this.cameraPreview.setColorEffect('mono');

            //console.log("Camera Ativada");
        } else if (oc == "clo") {
            const result = await this.cameraPreview.stopCamera();
            //console.log("Camera Desativada");
        }
    }*/

    async startCamera() { // inicia a camera

        this.picture = null;
        this.cameraPreview.startCamera(this.cameraOpts).then(
            (res) => {
                this.result1 = this.cameraPreview.setColorEffect('mono');
                this.takePicture();
            },
            (err) => {
                console.log(err)
            });
    }

    switchCamera() {
        this.cameraPreview.switchCamera();
    }

    async takePicture() {

        this.result1 = this.cameraPreview.setColorEffect('mono');
        this.cameraPreview.takePicture(this.cameraPictureOpts).then(
            (res) => {
                this.picture = 'data:image/jpeg;base64,' + res; // convertendo a imagem em data string jpeg
                this.loadImage();
                this.takePicture(); // chama recursivamente
            },
            (err) => {
                console.log(err)
            });
    }


    async refreshCanvas(image: MarvinImage) {
        image.draw(document.getElementById("canvasFilters"));
    }

    /**
     * Carrega os valores de um objeto MarvinImage que possibilita
     * pegar os valores da imagem.
     */
    async loadImage() {

        let self = this;
        this.originalImg = new MarvinImage();
        this.originalImg.load(this.picture, function () {
            self.outputImg = new MarvinImage(this.getWidth(), this.getHeight());
            self.itemSelected(self.filters[0]);
        });
    }

    async itemSelected(filter: Filter) {
        let img = filter.applyFilter(this.originalImg, this.outputImg);
        this.getPointSpr(img);
    }


    async setupFilters() {
        this.filters.push(new GrayScaleFilter("Gray Scale"));
    }


    async getPointSpr(image: MarvinImage) {
        let soma = [];
        let media = [];
        let dados = [];

        //Percorre o array para pegar as somas dos valores por coluna
        for (let i = 0; i < image.getHeight(); i++) {
            for (var j = 0; j < image.getWidth(); j++) {

                if (soma[j] == null) {
                    soma.push(image.getIntComponent0(i, j))
                } else {
                    soma[j] = soma[j] + image.getIntComponent0(i, j);
                }

            }
        }
        //Percorre o array para calcular a MÉDIA por COLUNA
        for (let i = 0; i < soma.length; i++) {
            media[i] = (soma[i] / parseInt(image.getWidth()));

        }
        if (this.currentIndicesDryCell.length == 0) {
            //verifica se os valores de referência estão vazios e setam o vetor
            this.currentIndicesDryCell = media; //Seta os valores de referência
        }

        let dadosAIM = [];
        for (let i = 0; i < media.length; i++) {
            dadosAIM.push(media[i] / this.currentIndicesDryCell[i]);
        }

        /**
         * O procedimento abaixo realiza a suavização por mediana,
         * passando os valores e o parametro de pontos vizinhos.
         **/
        console.log(this.data);

        if (this.data == undefined) {
            this.graficoAIM.series[0].setData(dadosAIM);
        } else {
            this.graficoAIM.series[0].setData(median(dadosAIM, this.data));
        }

        /**
         * A função a seguir realiza a normalização dos valores de referência.
         */

        this.normalizacao(this.currentIndicesDryCell);

        let min = 255.0;
        for (let i = 0; i < dadosAIM.length; i++) {
            if (min > dadosAIM[i]) { min = dadosAIM[i]; }
        }

        let max = 0;
        for (let j = 0; j < dadosAIM.length; j++) {
            if (max < dadosAIM[j]) {
                max = dadosAIM[j];
            }
        }

        let teta_medio = ((max + min) / 2);

        let aux = 0;
        let valorCL = 0;
        let valorCR = 0;
        for (let k = 0; k < dadosAIM.length; k++) {
            if (dadosAIM[k] <= teta_medio) {
                if (aux == 0) {
                    valorCL = dadosAIM[k];
                    aux++;
                } else {
                    valorCR = dadosAIM[k];
                }
            }
        }

        this.largura = parseFloat("" + (valorCL - valorCR).toFixed(2));
        this.assimetria = parseFloat("" + (valorCL / valorCR).toFixed(2));

        //console.log("Largura = " + this.largura);
        //console.log("Assimetria = " + this.assimetria);

        this.minimo = parseFloat("" + min.toFixed(2));

        this.chartSensorgrama.series[0].addPoint([min]);

    }

    /**
     * Este método zera o valores atuais de referência, permitindo que
     * novos valores sejam capturados e armazenados. 
     */
    dryCell() {
        this.currentIndicesDryCell = []; //Zera o vetor de índices de referência 
    }

    /**
     * 
     * Normaliza os dados de @param dadosAIM em valores entre 0 e 1
     * para que seja possível plotá-los no mesmo gráfico dos valores de
     * IR.
     */
    normalizacao(dadosAIM: number[]) {
        let min = 255.0;
        let max = 0;
        let dados = [];
        for (let i = 0; i < dadosAIM.length; i++) {
            if (min > dadosAIM[i]) { min = dadosAIM[i]; }
            if (max < dadosAIM[i]) { max = dadosAIM[i]; }
        }

        for (let j = 0; j < dadosAIM.length; j++) {
            dados.push(((dadosAIM[j] - min) / (max - min)));
        }

        //console.log(dados);

        //Descomente o codigo abaixo para apresentar no grafico
        this.graficoAIM.series[1].setData(dados);
        //console.log(dadosAIM);

        //return dadosAIM;
    }

    async alturaLargura() {
        //console.log("A função está sendo chamada");

        let alert = this.alertCtrl.create({
            title: 'Tamanho da Fonte de Luz',
            inputs: [
                {
                    name: 'altura',
                    placeholder: 'Altura:',
                    value: '100'
                },
                {
                    name: 'largura',
                    placeholder: 'Largura:',
                    value: '140'
                },
                {
                    name: 'posicao',
                    placeholder: 'Posicao:',
                    value: 'Esquerda'
                }
            ],
            buttons: [
                {
                    text: 'Salvar',
                    handler: data => {
                        let valorTamanho = document.getElementById("card-color");
                        let tAltura = data.altura;
                        let tLargura = data.largura;
                        valorTamanho.style.width = `${tLargura}px`;
                        valorTamanho.style.height = `${tAltura}px`;

                        if (data.posicao == "Direita") {
                            valorTamanho.style.cssFloat = "right";
                            valorTamanho.style.paddingTop = "0px";
                        } else if (data.posicao == "Esquerda") {
                            valorTamanho.style.cssFloat = "left";
                            valorTamanho.style.paddingTop = "0px";
                        } else {
                            valorTamanho.style.cssFloat = "none";
                            valorTamanho.style.paddingTop = "0px";
                        }
                    }
                }, {
                    text: 'Cancelar',
                    role: 'cancelar'
                }
            ]
        });

        alert.present();
    }

    enable(onOff: string, fab: FabContainer) {
        this.fab = fab;
        // let bar = document.getElementById("ligth-bar");
        fab.close();
        //console.log("Value: ", onOff);
        //document.createTextNode()
    }

    /**
     * Esconde o elemento [knob] de regulagem de luz.
     */
    clickHide() {
        //let bar = document.getElementById("ligth-bar");
        $('#hide').click(function () {
            $('ion-card-content').hide();
        });
    }

    /**
     * Esconde o elemento da fonte de luz. 
     */
    clickImage() {
        $('#cardImage').click(function () {
            $('ion-card').hide();
        });
    }

    /**
   * Exibe o elemento [knob] de regulagem de luz.
   */
    clickShow() {
        $('#show').click(function () {
            $('ion-card-content').show();
        });
    }

    /* Métodos para dialogos sobre Mediana*/
    public chamarMediana() {
        let alert = this.alertCtrl.create();
        alert.setTitle('Deseja aplicar a Mediana?');

        alert.addInput({
            type: 'radio',
            label: 'Mediana',
            value: 'ok',
            checked: false
        });

        alert.addInput({
            type: 'radio',
            label: 'Sem Mediana',
            value: 'undefined',
            checked: false
        });

        alert.addButton({
            text: 'Cancelar'
        });
        alert.addButton({
            text: 'OK',
            handler: data => {
                this.testRadioOpen = false;
                if (data == "ok") {
                    this.dialogMediana();
                } else if (data == "undefined") {
                    this.data = undefined;
                }
            }
        });
        alert.present();
    }

    dialogMediana() {
        const prompt = this.alertCtrl.create({
            title: 'Valor da mediana',
            message: "Digite o valor a ser aplicado",
            inputs: [
                {
                    name: 'mediana',
                    placeholder: 'Valor Mediadan',
                    type: 'number'
                },
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    handler: data => {
                        console.log('Cancelar clicked');
                    }
                },
                {
                    text: 'Salvar',
                    handler: data => {
                        console.log('Executar o metodo aqui!!!');
                        console.log(data.mediana);
                        this.data = data.mediana;
                    }
                }
            ]
        });
        prompt.present();

    }


}
